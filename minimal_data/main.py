#!/usr/bin/env python

# ECE 772 - Neural Networks and Learning Machines
# Term Project: SVM Image Reconstruction
# minimal dataset
# 
# Kampelmuehler, Moritz (400108331)
# Yayli, Melih (400096452)
# 12/2016

#imports
import numpy as np
import json
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.externals import joblib
import time
import random
import sys

# load data
with open('lena_data_s2.json', 'r') as f:
	raw_data = json.load(f)
	black_pixels = np.array(raw_data['black'])
	white_pixels = np.array(raw_data['white'])

print('sample size: ' + str(np.shape(black_pixels)[0]))

# oneclass svm (uncomment if new model is to be trained)
clf = svm.OneClassSVM(nu=1e-3, kernel="rbf", gamma=.0035,  max_iter=1e4)
clf.fit(black_pixels)

# dump/load
#~ joblib.dump(clf, 'trained_svm.pkl')
#~ clf = joblib.load('trained_svm.pkl')

# plotting
xx, yy = np.meshgrid(np.linspace(0, 512, 512), np.linspace(0, 512, 512))

Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
Z = Z.reshape(xx.shape)

plt.title("SVM fit")
a = plt.contour(xx, yy, Z, levels=[0], linewidths=2, colors='black')
plt.contourf(xx, yy, Z, levels=[0, Z.max()], colors='black')
plt.gca().invert_yaxis()
plt.gca().set_aspect('equal', adjustable='box')
#~ plt.savefig(str(i) + '_iter.png')
plt.show()

