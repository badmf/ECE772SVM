#!/usr/bin/env python

# ECE 772 - Neural Networks and Learning Machines
# Term Project: SVM Image Reconstruction
# 5 greylevels dataset
# 
# Kampelmuehler, Moritz (400108331)
# Yayli, Melih (400096452)
# 12/2016

#imports
import numpy as np
import json
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.externals import joblib
import time
import random
import sys
from matplotlib import colors as c

image_size = 256

hat_color = '#e3cbc1'
feather_color = '#b6b3df'
skin_color = '#eeb8a3'
bg_color = '#fff5ef'
contour_color = '0.0'

cMap = c.ListedColormap([feather_color, hat_color, skin_color, bg_color, contour_color])

# load data
with open('lena_data_gs5.json', 'r') as f:
	raw_data = json.load(f)
	black_pixels = np.array(raw_data['black'])
	white_pixels = np.array(raw_data['white'])
	hat_pixels = np.array(raw_data['hat'])
	feather_pixels = np.array(raw_data['feather'])
	skin_pixels = np.array(raw_data['skin'])
	
sample_size = 3000

hat_pixels = random.sample(list(hat_pixels), sample_size)
feather_pixels = random.sample(list(feather_pixels), sample_size)
skin_pixels = random.sample(list(skin_pixels), sample_size)
black_pixels = random.sample(list(black_pixels), sample_size)
white_pixels = random.sample(list(white_pixels), sample_size)

X = np.vstack((feather_pixels, hat_pixels, skin_pixels, black_pixels, white_pixels))
Y = np.hstack((np.ones(np.shape(feather_pixels)[0]), np.ones(np.shape(black_pixels)[0])*2, np.ones(np.shape(white_pixels)[0])*3, np.ones(np.shape(hat_pixels)[0])*4, np.ones(np.shape(skin_pixels)[0])*5))

# oneclass svm (uncomment if new model is to be trained)
clf = svm.SVC(gamma=0.1)
clf.fit(X, Y)

# dump/load
#~ joblib.dump(clf, 'trained_svm_'+ str(sample_size) + '.pkl')
#~ clf = joblib.load('trained_svm.pkl')

# plotting
xx, yy = np.meshgrid(np.linspace(0, image_size, image_size), np.linspace(0, image_size, image_size))

Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
Z = Z.reshape(xx.shape)

plt.pcolormesh(xx, yy, Z, cmap=cMap)

# Plot also the training points
#~ plt.scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.Paired)
plt.title('5-class SVM')
plt.axis('tight')
plt.gca().invert_yaxis()
plt.gca().set_aspect('equal', adjustable='box')
plt.show()
